# External DNS - Least Privilege https://github.com/kubernetes-sigs/external-dns/issues/2005#issuecomment-795439405
resource "google_project_iam_custom_role" "external_dns" {
  role_id     = "external_dns_${replace(var.environment, "-", "_")}"
  title       = "External DNS Access"
  description = "Access for external-dns to manage dns in the project"
  permissions = [
    "dns.projects.get",
    "dns.managedZones.get",
    "dns.managedZones.list",
    "dns.resourceRecordSets.list",
    "dns.resourceRecordSets.create",
    "dns.resourceRecordSets.delete",
    "dns.resourceRecordSets.update",
    "dns.changes.list",
    "dns.changes.get",
    "dns.changes.create",
  ]
}

# Workload Identity SA
resource "google_service_account" "kube_external_dns" {
  for_each = var.clusters

  project     = var.project
  account_id  = "k8s-dns-${each.key}"
  description = "A service account for use by GKE external-dns."
}

resource "google_project_iam_member" "kube_external_dns" {
  for_each = var.clusters

  project = var.project
  role    = google_project_iam_custom_role.external_dns.name
  member  = "serviceAccount:${google_service_account.kube_external_dns[each.key].email}"
}

resource "google_service_account_iam_member" "kube_external_dns_workload_identity" {
  for_each = var.clusters

  service_account_id = google_service_account.kube_external_dns[each.key].name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.project}.svc.id.goog[${each.value.k8s_namespace}/${each.value.k8s_sa_name}]"
}
