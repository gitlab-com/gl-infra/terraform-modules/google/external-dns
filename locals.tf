locals {
  cloud_provider = "gke"

  generic_dns_name         = "%s.${local.cloud_provider}.${var.domain}."
  generic_dns_name_private = "%s-private.${local.cloud_provider}.${var.domain}."

  project_dns_name = "${var.project}.${local.cloud_provider}.${var.domain}."
  project_name     = trimsuffix(replace(local.project_dns_name, ".", "-"), "-")

  location_dns_name = "%s.${var.project}.${local.cloud_provider}.${var.domain}."
  location_name     = trimsuffix(replace(local.location_dns_name, ".", "-"), "-")

  cluster_dns_name = "%s.%s.${var.project}.${local.cloud_provider}.${var.domain}."
  cluster_name     = trimsuffix(replace(local.cluster_dns_name, ".", "-"), "-")

  locations = toset([for v in values(var.clusters) : v.location])
}
