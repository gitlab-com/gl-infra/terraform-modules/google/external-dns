###
# Generic DNS zone
# DEPRECATED: use cluster zone below
###
resource "google_dns_managed_zone" "generic_dns_zone" {
  for_each = { for k, v in var.clusters : k => v if v.generic_zone_enabled }

  # Backwards compatibility
  name     = each.value.generic_zone_name != null ? "k8-ext-dns-${each.value.generic_zone_name}" : "kube-external-dns-${var.environment}"
  dns_name = format(local.generic_dns_name, coalesce(each.value.generic_zone_name, var.environment))
  labels   = var.labels
}

resource "google_dns_managed_zone" "generic_dns_zone_private" {
  for_each = { for k, v in var.clusters : k => v if v.generic_zone_enabled }

  # Backwards compatibility
  name       = each.value.generic_zone_name != null ? "k8-ext-dns-${each.value.generic_zone_name}-private" : "kube-external-dns-${var.environment}-private"
  dns_name   = format(local.generic_dns_name_private, coalesce(each.value.generic_zone_name, var.environment))
  labels     = var.labels
  visibility = "private"

  private_visibility_config {
    networks {
      network_url = var.network
    }
  }
}

resource "cloudflare_record" "generic_dns_zone_delegation" {
  for_each = { for record in flatten([
    # There are always 4 NS servers and can't use for_each here to avoid bootstrap issues
    for k, v in var.clusters : [
      for i in range(4) : [
        merge(v, { name = k, i = i })
      ] if v.generic_zone_enabled
    ]
    ]) :
    "${record.name}-${record.i}" => record
  }

  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = format(local.generic_dns_name, coalesce(each.value.generic_zone_name, var.environment))
  type    = "NS"
  ttl     = 3600
  content = trimsuffix(lower(google_dns_managed_zone.generic_dns_zone[each.value.name].name_servers[each.value.i]), ".")
}

resource "cloudflare_record" "generic_dns_zone_delegation_private" {
  for_each = { for record in flatten([
    # There is 1 NS private server and can't use for_each here to avoid bootstrap issues
    for k, v in var.clusters : [
      for i in range(1) : [
        merge(v, { name = k, i = i })
      ] if v.generic_zone_enabled
    ]
    ]) :
    "${record.name}-${record.i}" => record
  }

  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = format(local.generic_dns_name_private, coalesce(each.value.generic_zone_name, var.environment))
  type    = "NS"
  ttl     = 3600
  content = trimsuffix(lower(google_dns_managed_zone.generic_dns_zone_private[each.value.name].name_servers[each.value.i]), ".")
}

###
# Project DNS zone
###
resource "google_dns_managed_zone" "project_zone" {
  name = local.project_name

  dns_name = local.project_dns_name
  labels   = var.labels
}

resource "cloudflare_record" "project_zone_delegation" {
  # There are always 4 NS servers and can't use for_each here to avoid bootstrap issues
  count = 4

  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = local.project_dns_name
  type    = "NS"
  ttl     = 3600
  content = trimsuffix(lower(google_dns_managed_zone.project_zone.name_servers[count.index]), ".")
}

###
# Location/Region/Zonal DNS zone
###
resource "google_dns_managed_zone" "location_zone" {
  for_each = local.locations

  depends_on = [
    google_dns_managed_zone.project_zone,
  ]

  project  = var.project
  name     = format(local.location_name, each.key)
  dns_name = format(local.location_dns_name, each.key)
  labels   = var.labels
}

resource "google_dns_record_set" "location_zone_delegation" {
  for_each = local.locations

  name         = google_dns_managed_zone.location_zone[each.key].dns_name
  managed_zone = google_dns_managed_zone.project_zone.name
  type         = "NS"
  ttl          = 3600
  rrdatas      = [for i in range(4) : lower(google_dns_managed_zone.location_zone[each.key].name_servers[i])]
}

###
# Cluster DNS zone
###
resource "google_dns_managed_zone" "cluster_zone" {
  for_each = var.clusters

  depends_on = [
    google_dns_managed_zone.location_zone,
  ]

  name     = format(local.cluster_name, each.key, each.value.location)
  dns_name = format(local.cluster_dns_name, each.key, each.value.location)
  labels   = var.labels
}

resource "google_dns_record_set" "cluster_zone_delegation" {
  for_each = var.clusters

  project      = var.project
  name         = google_dns_managed_zone.cluster_zone[each.key].dns_name
  managed_zone = google_dns_managed_zone.location_zone[each.value.location].name
  type         = "NS"
  ttl          = 3600
  rrdatas      = [for i in range(4) : lower(google_dns_managed_zone.cluster_zone[each.key].name_servers[i])]
}
