variable "environment" {
  type        = string
  description = "Environment name"
}

variable "project" {
  type        = string
  description = "GitLab cloud project name"
}

variable "domain" {
  type        = string
  description = "GitLab root domain name"
  default     = "gitlab.net"
}

variable "clusters" {
  type = map(object({
    location             = string
    k8s_sa_name          = optional(string, "external-dns")
    k8s_namespace        = optional(string, "external-dns")
    generic_zone_enabled = optional(bool, false)
    generic_zone_name    = optional(string)
  }))
  description = "Kubernetes clusters"
}

variable "network" {
  type        = string
  description = "The network that can see the private DNS zone"
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}
